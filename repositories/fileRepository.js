const moment = require('moment');
const { File } = require('../models/file');

class FileRepository {

    // Create a new file record in the database.
    async create(data) {
        // Create a new instance of the File model with the provided data
        const file = new File(data);
        // Save the new file record to the database
        return file.save();
    }

    // Get a file record by its public key.
    async getByPublicKey(publicKey) {
        // Use the findOne method of the File model to search for a file by publicKey
        return File.findOne({ publicKey });
    }

    // Get a file record by its private key.
    async getByPrivateKey(privateKey) {
        // Use the findOne method of the File model to search for a file by privateKey
        return File.findOne({ privateKey });
    }

    // Get files that are older than retention period.
    async getFilesOlderThanRetentionPeriod(retentionPeriod) {
        const retentionDate = moment().subtract(retentionPeriod, 'days').toDate();
        return File.find({ createdAt: { $lt: retentionDate } });
    }

    // Delete a file record by its UUID.
    async deleteByUUID(uuid) {
        // Use the deleteOne method of the File model to delete a file by its UUID
        return File.deleteOne({ uuid });
    }

}

module.exports = FileRepository;
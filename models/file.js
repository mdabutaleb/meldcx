const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');

// Define a schema for the File model
const schema = new mongoose.Schema({
    uuid: {
        type: String,
        unique: true, // Ensures uniqueness
        defaultValue: uuidv4(), // Generates a UUID as the default value
    },
    name: {
        type: String,
        maxlength: 100,
        required: true, // Required field
    },
    size: {
        type: String,
    },
    mimeType: {
        type: String,
        required: true, // Required field
    },
    path: {
        type: String,
    },
    privateKey: {
        type: String,
        unique: true, // Ensures uniqueness
    },
    publicKey: {
        type: String,
        unique: true, // Ensures uniqueness
    },
    signature: { // Will be used for validating public/private keys
        type: String,
        unique: true, // Ensures uniqueness
    },
    createdAt: {
        type: Date,
        default: Date.now(), // Default value is the current date
    },
    updatedAt: {
        type: Date,
        default: Date.now(), // Default value is the current date
    }
});

// Create the File model based on the schema
const File = mongoose.model('File', schema);

// Export the File model for use in other parts of the application
module.exports.File = File;
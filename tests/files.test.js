const fs = require('fs');
const axios = require('axios');
const FormData = require('form-data');
const path = require('path');

const basePath = 'http://app:3000';
const filesDir = path.join(__dirname, 'files');
const downloadedFilesDir = path.join(__dirname, 'downloaded_files');
const uploadsDir = path.join(__dirname, '../uploads');

// Before all tests, set up the environment
beforeAll(() => {
    // Create necessary directories if they don't exist
    createDirectoryIfNotExists(filesDir);
    createDirectoryIfNotExists(downloadedFilesDir);

    // Create a test file
    const testFilePath = path.join(filesDir, 'test_file.txt');
    fs.writeFileSync(testFilePath, 'ABC');
    console.log('File created successfully.');
});

// After all tests, clean up the environment
afterAll(() => {
    // Remove directories and their contents
    removeDirectoryIfExists(filesDir);
    removeDirectoryIfExists(downloadedFilesDir);
    removeDirectoryIfExists(uploadsDir);
});

// Test suite for uploading files
describe('POST /files', () => {
    it('Should upload files.', async () => {
        const file = path.join(filesDir, 'test_file.txt');
        const formData = new FormData();
        formData.append('file', fs.createReadStream(file));

        const response = await axios.post(`${basePath}/files`, formData, {
            headers: formData.getHeaders(),
        });

        expect(response?.data?.data ?? {}).toHaveProperty('publicKey');
        expect(response?.data?.data ?? {}).toHaveProperty('privateKey');
        expect(response.status).toBe(201);
    });
});

// Test suite for downloading files
describe('GET /files/:publicKey', () => {
    it('Should download file with public key.', async () => {
        const file = path.join(filesDir, 'test_file.txt');
        const formData = new FormData();
        formData.append('file', fs.createReadStream(file));

        const uploadResponse = await axios.post(`${basePath}/files`, formData, {
            headers: formData.getHeaders(),
        });

        const downloadResponse = await axios.get(`${basePath}/files/${uploadResponse.data.data.publicKey}`, {
            responseType: 'stream',
        });

        const outputPath = path.join(downloadedFilesDir, 'test_file.txt');
        const outputStream = fs.createWriteStream(outputPath);
        downloadResponse.data.pipe(outputStream);

        await new Promise((resolve, reject) => {
            outputStream.on('finish', resolve);
            outputStream.on('error', reject);
        });

        expect(downloadResponse.status).toBe(200);
    });

    it('Should not download file with private/invalid key.', async () => {
        const file = path.join(filesDir, 'test_file.txt');
        const formData = new FormData();
        formData.append('file', fs.createReadStream(file));

        const uploadResponse = await axios.post(`${basePath}/files`, formData, {
            headers: formData.getHeaders(),
        });

        try {
            await axios.get(`${basePath}/files/${uploadResponse.data.data.privateKey}`);
            // If this line is reached, the test should fail
            fail('Expected request to fail');
        } catch (error) {
            expect(error.response.status).not.toBe(200);
        }
    });
});

// Test suite for deleting files
describe('DELETE /files/:privateKey', () => {
    it('Should delete file with private key.', async () => {
        const file = path.join(filesDir, 'test_file.txt');
        const formData = new FormData();
        formData.append('file', fs.createReadStream(file));

        const uploadResponse = await axios.post(`${basePath}/files`, formData, {
            headers: formData.getHeaders(),
        });

        const deleteResponse = await axios.delete(`${basePath}/files/${uploadResponse.data.data.privateKey}`);

        expect(deleteResponse.status).toBe(200);
    });

    it('Should not delete file with public/invalid key.', async () => {
        const file = path.join(filesDir, 'test_file.txt');
        const formData = new FormData();
        formData.append('file', fs.createReadStream(file));

        const uploadResponse = await axios.post(`${basePath}/files`, formData, {
            headers: formData.getHeaders(),
        });

        try {
            await axios.delete(`${basePath}/files/${uploadResponse.data.data.publicKey}`);
            // If this line is reached, the test should fail
            fail('Expected request to fail');
        } catch (error) {
            expect(error.response.status).not.toBe(200);
        }
    });
});

// Create a directory if it doesn't exist
function createDirectoryIfNotExists(directoryPath) {
    if (!fs.existsSync(directoryPath)) {
        fs.mkdirSync(directoryPath, { recursive: true });
        console.log(`Directory created successfully: ${directoryPath}`);
    } else {
        console.log(`Directory already exists: ${directoryPath}`);
    }
}

// Remove a directory if it exists
function removeDirectoryIfExists(directoryPath) {
    if (fs.existsSync(directoryPath)) {
        fs.rmdirSync(directoryPath, { recursive: true });
        console.log(`Directory deleted successfully: ${directoryPath}`);
    } else {
        console.log(`Directory does not exist: ${directoryPath}`);
    }
}
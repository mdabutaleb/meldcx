const rateLimit = require('express-rate-limit');

// Define a rate limit for successful uploads
const uploadLimit = rateLimit({
    windowMs: 24 * 60 * 60 * 1000, // 24 hours
    max: process.env.DAILY_UPLOAD_LIMIT || 100, // Max successful responses per day per IP address
    skipFailedRequests: true, // Skip rate limiting for failed (non-2xx) responses
});

// Define a rate limit for successful uploads
const downloadLimit = rateLimit({
    windowMs: 24 * 60 * 60 * 1000, // 24 hours
    max: process.env.DAILY_DOWNLOAD_LIMIT || 100, // Max successful responses per day per IP address
    skipFailedRequests: true, // Skip rate limiting for failed (non-2xx) responses
});

module.exports = {
    uploadLimit,
    downloadLimit
};

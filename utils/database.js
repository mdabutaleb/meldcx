const mongoose = require('mongoose');

// Create a separate configuration object for the database connection
const dbConfig = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

// Connect to the database
async function connectToDatabase() {


    try {
        if (process.env.MONGO_URI) {
            await mongoose.connect(process.env.MONGO_URI, dbConfig);

            console.log('Connected to the database.');
        } else {
            console.error('MONGO_URI is missing from environment variables.');
            process.exit(1);
        }
    } catch (error) {
        console.error('Database connection error:', error);
        process.exit(1);
    }
}

module.exports = {
    connectToDatabase,
    db: mongoose.connection,
};

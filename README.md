
# Docker Based Installation

**Step 1: Install Dependencies**
- Install the necessary packages by running this command:
  ```bash
  docker compose run --rm app npm install
  ```
  This will install essential packages like Express, Mongoose, Multer, and more.

**Step 2: Configure Environment Variables**
- Copy the `.env.example` file to create a new `.env` file:
  ```bash
  docker compose run --rm app cp .env.example .env
  ```
  Customize the variables in the `.env` file to match your application's requirements.

**Step 3: Start the Application**
- To run your application in development mode, use the predefined script with `nodemon`:
  ```bash
  docker compose up -d
  ```
  Your application will automatically restart when you make code changes.

**Step 4: Access Your Application**
- Open a web browser [Web Browser](http://localhost:3000/) or a tool like [Postman](https://www.postman.com/) to test your API endpoints.
- The application defaults to running on port 3000, as specified in the `.env` file. Adjust this port as needed.
-
**Step 5: Run test**
- Run the test case by the following commands:
  ```bash
  docker compose run --rm app npm test
  ```
# Manual Installation

**Step 1: Install Dependencies**
- Install the necessary packages by running this command:
  ```bash
  npm install
  ```
  This will install essential packages like Express, Mongoose, Multer, and more.

**Step 2: Configure Environment Variables**
- Copy the `.env.example` file to create a new `.env` file:
  ```bash
  cp .env.example .env
  ```
  Customize the variables in the `.env` file to match your application's requirements.

**Step 3: Start the Application**
- To run your application in development mode, use the predefined script with `nodemon`:
  ```bash
  npm run dev
  ```
  Your application will automatically restart when you make code changes.

**Step 4: Access Your Application**
- Open a web browser or a tool like [Postman](https://www.postman.com/) to test your API endpoints.
- The application defaults to running on port 3000, as specified in the `.env` file. Adjust this port as needed.
require('dotenv').config();
const cron = require('node-cron');
const express = require('express');
const bodyParser = require('body-parser');
const CronService = require("./services/cronService");
const {connectToDatabase} = require('./utils/database');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// api routes
app.use('/', require('./routes/api'));
app.use((req, res, next) => {
    res.status(404).json({
        status: 'error',
        code: 404,
        message: 'Not Found'
    });
});

// global unhandled exception handler
process.on('unhandledRejection', (reason, promise) => {
    console.error('Unhandled rejection at:', promise, 'reason:', reason);
    process.exit(1);
});

// global uncaught exception handler
process.on('uncaughtException', (error) => {
    console.error('Uncaught exception:', error);
    process.exit(1);
});

// Connect to the database and start the Express server
connectToDatabase().then(() => {
    app.listen(process.env.APP_PORT || 3000, () => {
        console.log(`Server is running on port ${process.env.APP_PORT || 3000}`);
    });

    // Schedule a cron job to clean old files every minute
    cron.schedule('* * * * *', async () => {
        await (new CronService().cleanOldFiles());
    });
});
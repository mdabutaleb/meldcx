// Import required modules and services
const path = require("path");
const { v4: uuidv4 } = require('uuid');
const CryptoService = require('../services/cryptoService');
const StorageFactory = require("../services/storage/storageFactory");
const FileRepository = require("../repositories/fileRepository");

// Store an uploaded file
async function storeFile(req, res) {
    try {
        // Check if a file is included in the request
        if (!req.file) {
            return res.status(422).send({
                status: 'error',
                code: 422,
                message: 'File is required.',
            });
        }

        // Generate a unique identifier (UUID) for the file
        const uuid = uuidv4();

        // Create a storage instance based on the configured storage type
        const storage = (new StorageFactory()).create(process.env.STORAGE_DISK || 'local');

        // Store the uploaded file using the storage service
        const uploadedFile = await storage.store(req.file, uuid);

        // Create a key pair for encryption and generate a signature
        const cryptoService = new CryptoService();
        const keyPair = cryptoService.createKeyPair();
        const signature = cryptoService.createSignature(uuid, keyPair.privateKey);

        // Prepare data to be stored in the database
        const data = {
            uuid: uuid,
            name: uploadedFile.name,
            size: uploadedFile.size,
            mimeType: uploadedFile.mimeType,
            path: uploadedFile.path,
            publicKey: Buffer.from(keyPair.publicKey).toString('base64'),
            privateKey: Buffer.from(keyPair.privateKey).toString('base64'),
            signature: signature
        };

        // Create a new record in the database with the file data
        const fileRepo = new FileRepository();
        await fileRepo.create(data);

        // Return a success response with public and private keys
        return res.status(201).send({
            status: "success",
            code: 201,
            message: "File uploaded successfully!",
            data: {
                publicKey: data.publicKey,
                privateKey: data.privateKey
            }
        });
    } catch (error) {
        // Handle any errors and return an error response
        return handleError(res, error);
    }
}

// Download a file using its public key
async function downloadFile(req, res) {
    try {
        // Get the public key from the request parameters
        const publicKey = req.params.publicKey;
        if (!publicKey) {
            return res.status(422).send({
                status: "error",
                code: 422,
                message: "Please provide a public key with URL."
            });
        }

        // Retrieve file information from the database using the public key
        const fileRepo = new FileRepository();
        const file = await fileRepo.getByPublicKey(publicKey);

        // Check if the file exists
        if (!file) {
            return res.status(404).send({
                status: "error",
                code: 404,
                message: "Not found or invalid public key"
            });
        }

        // Verify the authenticity of the public key and file signature
        const cryptoService = new CryptoService();
        const decodedPublicKey = Buffer.from(publicKey, 'base64').toString('utf8');
        const isVerified = await cryptoService.verifySignature(file.uuid, file.signature, decodedPublicKey);

        // If verification fails, return an error response
        if (!isVerified) {
            return res.status(400).send({
                status: "error",
                code: 400,
                message: "Public key not valid."
            });
        }

        // Set response headers for file download
        res.setHeader('Content-Type', file.mimeType);
        res.setHeader('Content-Disposition', `attachment; filename="${file.name}"`);

        // Send the file contents as the response
        return res.sendFile(path.join(__dirname, '../', file.path));
    } catch (error) {
        // Handle any errors and return an error response
        return handleError(res, error);
    }
}

// Delete a file using its private key
async function deleteFile(req, res) {
    try {
        // Get the private key from the request parameters
        const privateKey = req.params.privateKey;
        if (!privateKey) {
            return res.status(422).send({
                status: "error",
                code: 422,
                message: "Please provide a private key with URL."
            });
        }

        // Retrieve file information from the database using the private key
        const fileRepo = new FileRepository();
        const file = await fileRepo.getByPrivateKey(privateKey);

        // Check if the file exists
        if (!file) {
            return res.status(404).send({
                status: "error",
                code: 404,
                message: "Private key not found in the database."
            });
        }

        // Verify the authenticity of the associated public key and file signature
        const cryptoService = new CryptoService();
        const decodedPublicKey = Buffer.from(file.publicKey, 'base64').toString('utf8');
        const isVerified = await cryptoService.verifySignature(file.uuid, file.signature, decodedPublicKey);

        // If verification fails, return an error response
        if (!isVerified) {
            return res.status(400).send({
                status: "error",
                code: 400,
                message: "Private key not valid."
            });
        }

        // Create a storage instance based on the configured storage type
        const storage = (new StorageFactory()).create(process.env.STORAGE_DISK || 'local');

        // Delete the file using the storage service
        const removedFile = await storage.delete(file.path);

        // Check if the file deletion was successful
        if (!removedFile) {
            return res.status(400).send({
                status: "error",
                code: 400,
                message: "File couldn't be deleted."
            });
        }

        // Delete the file record from the database
        const deletedFile = await file.deleteOne();

        // Check if the file record deletion was successful
        if (!deletedFile) {
            return res.status(400).send({
                status: "error",
                code: 400,
                message: "File couldn't be deleted."
            });
        }

        // Return a success response
        return res.status(200).send({
            status: "success",
            code: 200,
            message: "File has been deleted."
        });
    } catch (error) {
        // Handle any errors and return an error response
        return handleError(res, error);
    }
}

// Handle errors and return an error response
function handleError(res, error) {
    console.error(error);

    return res.status(500).send({
        status: "error",
        code: 500,
        message: error.message || "Internal Server Error"
    });
}

module.exports = {
    store: storeFile,
    download: downloadFile,
    delete: deleteFile
};

const path = require('path');
const fs = require('fs').promises;
const StorageServiceInterface = require("./storageServiceInterface");

// Define the default storage path, which can be overridden by the STORAGE_PATH environment variable
const storagePath = process.env.STORAGE_PATH || 'uploads';

class LocalStorageService extends StorageServiceInterface {

    // Store a file with a given UUID
    async store(file, uuid) {
        // Split the original file name to extract the file extension
        const parts = file.originalname.split('.');
        const fileExt = parts[parts.length - 1];

        // Generate a unique file name using the UUID and file extension
        const fileName = `${uuid}.${fileExt}`;

        // Construct the full file path based on the storage path and generated file name
        const filePath = path.join(storagePath, fileName);

        // Ensure that the directory exists or create it if not
        await this.createDirectoryIfNotExists(storagePath);

        // Write the file content (buffer) to the specified file path
        await fs.writeFile(filePath, file.buffer);

        // Return information about the stored file
        return { name: fileName, path: filePath, mimeType: file.mimetype, size: file.size };
    }

    // Get the content of a file from the specified file path
    async get(filePath) {
        // Ensure that the directory exists or create it if not
        await this.createDirectoryIfNotExists(storagePath);

        try {
            // Read the content of the file at the given file path
            return await fs.readFile(filePath);
        } catch (error) {
            // If the file is not found, throw an error indicating that the file was not found
            if (error.code === 'ENOENT') {
                throw new Error('File not found');
            }

            // For other errors, re-throw the original error
            throw error;
        }
    }

    // Delete a file at the specified file path
    async delete(filePath) {
        // Ensure that the directory exists or create it if not
        await this.createDirectoryIfNotExists(storagePath);

        try {
            // Attempt to unlink (delete) the file at the specified path
            await fs.unlink(filePath);

            // Return true to indicate that the file was successfully deleted
            return true;
        } catch (error) {
            // If the file is not found, throw an error indicating that the file was not found
            if (error.code === 'ENOENT') {
                throw new Error('File not found');
            }

            // For other errors, re-throw the original error
            throw error;
        }
    }

    // Create a directory if it does not exist at the specified directory path
    async createDirectoryIfNotExists(directoryPath) {
        try {
            // Attempt to access the directory to check if it exists
            await fs.access(directoryPath);
        } catch (error) {
            // If the directory does not exist, create it recursively
            if (error.code === 'ENOENT') {
                await fs.mkdir(directoryPath, { recursive: true });
            } else {
                // For other errors, re-throw the original error
                throw error;
            }
        }
    }

}

module.exports = LocalStorageService;
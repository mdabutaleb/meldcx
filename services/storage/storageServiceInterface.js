/**
 * @interface
 * @name StorageServiceInterface
 * @description Interface representing Storage Service.
 */
class StorageServiceInterface {

    /**
     * Method to store a file in Storage.
     * @param {File} file - The file to be stored.
     * @param {string} uuid - The unique identifier for the file.
     * @throws {Error} If the method is not implemented.
     * @returns {Promise}
     * @member StorageServiceInterface
     */
    async store(file, uuid) {
        throw new Error('Method not implemented');
    }

    /**
     * Method to get a file from Storage.
     * @param {string} filePath - The path to the file.
     * @throws {Error} If the method is not implemented.
     * @returns {Promise} The retrieved file.
     * @member StorageServiceInterface
     */
    async get(filePath) {
        throw new Error('Method not implemented');
    }

    /**
     * Method to delete a file from Storage.
     * @param {string} filePath - The path to the file to be deleted.
     * @throws {Error} If the method is not implemented.
     * @returns {Promise}
     * @member StorageServiceInterface
     */
    async delete(filePath) {
        throw new Error('Method not implemented');
    }

}

module.exports = StorageServiceInterface;

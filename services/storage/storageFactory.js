const LocalStorageService = require('./localStorageService');
const GoogleStorageService = require('./googleStorageService');

class StorageFactory {

    // Creating a storage service based on the provided disk type
    create(disk) {
        switch (disk) {
            // If the disk type is 'local', create and return a LocalStorageService instance
            case 'local':
                return new LocalStorageService();

            // If the disk type is 'google', create and return a GoogleStorageService instance
            case 'google':
                return new GoogleStorageService();

            // If the disk type is not matched, throw an error indicating that the storage disk is not supported
            default:
                throw new Error('Unsupported storage disk.');
        }
    }

}

module.exports = StorageFactory;

const FileRepository = require("../repositories/fileRepository");
const StorageFactory = require("./storage/storageFactory");

class CronService {

    // Clean up old files based on the retention period.
    async cleanOldFiles() {
        try {
            // Create an instance of the FileRepository
            const fileRepo = new FileRepository();

            // Get files older than the configured retention period (default: 7 days)
            const retentionPeriodInDays = process.env.RETENTION_PERIOD || 7;
            const files = await fileRepo.getFilesOlderThanRetentionPeriod(retentionPeriodInDays);

            if (files.length > 0) {
                console.log(`Found ${files.length} records to delete.`);

                // Iterate through each file and delete it from the database and storage
                for (const file of files) {
                    const filePath = file.path;

                    // Delete the file record from the database
                    await fileRepo.deleteByUUID(file.uuid);

                    // Create a storage instance based on the configured storage type
                    const storage = (new StorageFactory()).create(process.env.STORAGE_DISK || 'local');

                    // Delete the file from storage
                    await storage.delete(filePath);
                }

                console.log('Deleted old files successfully.');
            } else {
                console.log('No records found to delete.');
            }
        } catch (error) {
            console.error('Error cleaning old files:', error);
        }
    }

}

module.exports = CronService;
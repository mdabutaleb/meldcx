const crypto = require('crypto');

class CryptoService {

    // Generate a new RSA key pair
    createKeyPair() {
        return crypto.generateKeyPairSync('rsa', {
            modulusLength: 2048,
            publicKeyEncoding: {
                type: 'spki',
                format: 'pem',
            },
            privateKeyEncoding: {
                type: 'pkcs8',
                format: 'pem',
            },
        });
    }

    // Create a digital signature for a given UUID using a private key
    createSignature(uuid, privateKey) {
        const sign = crypto.createSign('SHA256'); // Create a SHA-256 signer
        sign.write(uuid); // Write the UUID data
        sign.end(); // End the signing process

        return sign.sign(privateKey, 'base64'); // Sign the data using the private key and return it as base64
    }

    // Verify a digital signature for a given UUID and public key
    verifySignature(uuid, signature, publicKey) {
        const verify = crypto.createVerify('SHA256'); // Create a SHA-256 verifier
        verify.write(uuid); // Write the UUID data
        verify.end(); // End the verification process

        // Verify the signature against the UUID and public key, return true if valid, false otherwise
        return verify.verify(publicKey, signature, 'base64');
    }

}

module.exports = CryptoService;

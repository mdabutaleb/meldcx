const express = require('express');
const router = express.Router();
const multer = require('multer');
const {uploadLimit, downloadLimit} = require('../utils/ratelimiter');

// Set up multer for file handling
const upload = multer({
    storage: multer.memoryStorage(),
    fileFilter: (req, file, cb) => {
        // File size validation (limit to 5MB)
        if (file.size > 5 * 1024 * 1024) {
            return cb(new Error('File size exceeds the limit.'));
        }

        // If all validations pass, allow the file upload
        cb(null, true);
    },
});

// File routes
const fileController = require('../controllers/fileController');
router.post('/files', uploadLimit, upload.single('file'), fileController.store);
router.get('/files/:publicKey', downloadLimit, fileController.download);
router.delete('/files/:privateKey', fileController.delete);

module.exports = router;
